// Copyright Epic Games, Inc. All Rights Reserved.

#include "ImportBIMFInalThesisGameMode.h"
#include "ImportBIMFInalThesisCharacter.h"
#include "UObject/ConstructorHelpers.h"

AImportBIMFInalThesisGameMode::AImportBIMFInalThesisGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPerson/Blueprints/BP_FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

}
