// Fill out your copyright notice in the Description page of Project Settings.


#include "BlueprintHelpersFunctions.h"

bool UBlueprintHelpersFunctions::SplitStringByDelimiter(const FString& InputString, const FString& Delimiter, TArray<FString>& OutputArray)
{
    // Only clear the output array on the initial call
    if (OutputArray.Num() == 0)
    {
        OutputArray.Empty();
    }


    // Check if the Delimiter is only one char
    if (Delimiter.Len() != 1) {
        UE_LOG(LogTemp, Error, TEXT("Invalid delimiter length: %d"), Delimiter.Len());
        return false;
    }

    // Find the delimiter in the input string
    int32 DelimiterIndex = 0;
    if (InputString.FindChar(Delimiter[0], DelimiterIndex))
    {
        // Split the input string by the delimiter
        FString LeftPart = InputString.Left(DelimiterIndex);
        FString RightPart = InputString.RightChop(DelimiterIndex + Delimiter.Len());

        // Add the left part to the output array
        OutputArray.Add(LeftPart);

        if (RightPart.Len() == 0) {
            return true;
        }

        // Recursively split the right part
        SplitStringByDelimiter(RightPart, Delimiter, OutputArray);

        return true; // Operation succeeded
    }
    else
    {
        // If delimiter not found, add the input string to the output array
        OutputArray.Add(InputString);
        return true; // Operation succeeded
    }
}