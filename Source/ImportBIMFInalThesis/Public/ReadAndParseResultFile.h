// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "ReadAndParseResultFile.generated.h"

/**
 * 
 */
UCLASS()
class IMPORTBIMFINALTHESIS_API UReadAndParseResultFile : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
};
