// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "BlueprintHelpersFunctions.generated.h"

/**
 * 
 */
UCLASS()
class IMPORTBIMFINALTHESIS_API UBlueprintHelpersFunctions : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
    /**
     * Splits a string into an array of substrings based on a delimiter.
     * @param InputString The string to split.
     * @param Delimiter The string used to separate the substrings.
     * @param OutputArray (Out) The array to store the resulting substrings.
     */
    
     UFUNCTION(BlueprintCallable, Category = "MyHelpers")
        static bool SplitStringByDelimiter(const FString& InputString, const FString& Delimiter, TArray<FString>& OutputArray);
};
