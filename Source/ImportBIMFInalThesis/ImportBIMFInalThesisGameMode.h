// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ImportBIMFInalThesisGameMode.generated.h"

UCLASS(minimalapi)
class AImportBIMFInalThesisGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AImportBIMFInalThesisGameMode();
};



