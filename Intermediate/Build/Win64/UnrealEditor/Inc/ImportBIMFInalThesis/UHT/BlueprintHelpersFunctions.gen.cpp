// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ImportBIMFInalThesis/Public/BlueprintHelpersFunctions.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBlueprintHelpersFunctions() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	IMPORTBIMFINALTHESIS_API UClass* Z_Construct_UClass_UBlueprintHelpersFunctions();
	IMPORTBIMFINALTHESIS_API UClass* Z_Construct_UClass_UBlueprintHelpersFunctions_NoRegister();
	UPackage* Z_Construct_UPackage__Script_ImportBIMFInalThesis();
// End Cross Module References
	DEFINE_FUNCTION(UBlueprintHelpersFunctions::execSplitStringByDelimiter)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InputString);
		P_GET_PROPERTY(FStrProperty,Z_Param_Delimiter);
		P_GET_TARRAY_REF(FString,Z_Param_Out_OutputArray);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UBlueprintHelpersFunctions::SplitStringByDelimiter(Z_Param_InputString,Z_Param_Delimiter,Z_Param_Out_OutputArray);
		P_NATIVE_END;
	}
	void UBlueprintHelpersFunctions::StaticRegisterNativesUBlueprintHelpersFunctions()
	{
		UClass* Class = UBlueprintHelpersFunctions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SplitStringByDelimiter", &UBlueprintHelpersFunctions::execSplitStringByDelimiter },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics
	{
		struct BlueprintHelpersFunctions_eventSplitStringByDelimiter_Parms
		{
			FString InputString;
			FString Delimiter;
			TArray<FString> OutputArray;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_InputString_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_InputString;
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam NewProp_Delimiter_MetaData[];
#endif
		static const UECodeGen_Private::FStrPropertyParams NewProp_Delimiter;
		static const UECodeGen_Private::FStrPropertyParams NewProp_OutputArray_Inner;
		static const UECodeGen_Private::FArrayPropertyParams NewProp_OutputArray;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UECodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UECodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UECodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::NewProp_InputString_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::NewProp_InputString = { "InputString", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(BlueprintHelpersFunctions_eventSplitStringByDelimiter_Parms, InputString), METADATA_PARAMS(Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::NewProp_InputString_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::NewProp_InputString_MetaData)) };
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::NewProp_Delimiter_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::NewProp_Delimiter = { "Delimiter", nullptr, (EPropertyFlags)0x0010000000000080, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(BlueprintHelpersFunctions_eventSplitStringByDelimiter_Parms, Delimiter), METADATA_PARAMS(Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::NewProp_Delimiter_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::NewProp_Delimiter_MetaData)) };
	const UECodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::NewProp_OutputArray_Inner = { "OutputArray", nullptr, (EPropertyFlags)0x0000000000000000, UECodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, 0, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::NewProp_OutputArray = { "OutputArray", nullptr, (EPropertyFlags)0x0010000000000180, UECodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, STRUCT_OFFSET(BlueprintHelpersFunctions_eventSplitStringByDelimiter_Parms, OutputArray), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((BlueprintHelpersFunctions_eventSplitStringByDelimiter_Parms*)Obj)->ReturnValue = 1;
	}
	const UECodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UECodeGen_Private::EPropertyGenFlags::Bool | UECodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, nullptr, nullptr, sizeof(bool), sizeof(BlueprintHelpersFunctions_eventSplitStringByDelimiter_Parms), &Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UECodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::PropPointers[] = {
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::NewProp_InputString,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::NewProp_Delimiter,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::NewProp_OutputArray_Inner,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::NewProp_OutputArray,
		(const UECodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::Function_MetaDataParams[] = {
		{ "Category", "MyHelpers" },
		{ "Comment", "/**\n     * Splits a string into an array of substrings based on a delimiter.\n     * @param InputString The string to split.\n     * @param Delimiter The string used to separate the substrings.\n     * @param OutputArray (Out) The array to store the resulting substrings.\n     */" },
		{ "ModuleRelativePath", "Public/BlueprintHelpersFunctions.h" },
		{ "ToolTip", "Splits a string into an array of substrings based on a delimiter.\n@param InputString The string to split.\n@param Delimiter The string used to separate the substrings.\n@param OutputArray (Out) The array to store the resulting substrings." },
	};
#endif
	const UECodeGen_Private::FFunctionParams Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UBlueprintHelpersFunctions, nullptr, "SplitStringByDelimiter", nullptr, nullptr, sizeof(Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::BlueprintHelpersFunctions_eventSplitStringByDelimiter_Parms), Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UECodeGen_Private::ConstructUFunction(&ReturnFunction, Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UBlueprintHelpersFunctions);
	UClass* Z_Construct_UClass_UBlueprintHelpersFunctions_NoRegister()
	{
		return UBlueprintHelpersFunctions::StaticClass();
	}
	struct Z_Construct_UClass_UBlueprintHelpersFunctions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UBlueprintHelpersFunctions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_ImportBIMFInalThesis,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UBlueprintHelpersFunctions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UBlueprintHelpersFunctions_SplitStringByDelimiter, "SplitStringByDelimiter" }, // 1362462012
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UBlueprintHelpersFunctions_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "BlueprintHelpersFunctions.h" },
		{ "ModuleRelativePath", "Public/BlueprintHelpersFunctions.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UBlueprintHelpersFunctions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UBlueprintHelpersFunctions>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UBlueprintHelpersFunctions_Statics::ClassParams = {
		&UBlueprintHelpersFunctions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UBlueprintHelpersFunctions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UBlueprintHelpersFunctions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UBlueprintHelpersFunctions()
	{
		if (!Z_Registration_Info_UClass_UBlueprintHelpersFunctions.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UBlueprintHelpersFunctions.OuterSingleton, Z_Construct_UClass_UBlueprintHelpersFunctions_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UBlueprintHelpersFunctions.OuterSingleton;
	}
	template<> IMPORTBIMFINALTHESIS_API UClass* StaticClass<UBlueprintHelpersFunctions>()
	{
		return UBlueprintHelpersFunctions::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UBlueprintHelpersFunctions);
	UBlueprintHelpersFunctions::~UBlueprintHelpersFunctions() {}
	struct Z_CompiledInDeferFile_FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UBlueprintHelpersFunctions, UBlueprintHelpersFunctions::StaticClass, TEXT("UBlueprintHelpersFunctions"), &Z_Registration_Info_UClass_UBlueprintHelpersFunctions, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UBlueprintHelpersFunctions), 4280767152U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_561719449(TEXT("/Script/ImportBIMFInalThesis"),
		Z_CompiledInDeferFile_FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
