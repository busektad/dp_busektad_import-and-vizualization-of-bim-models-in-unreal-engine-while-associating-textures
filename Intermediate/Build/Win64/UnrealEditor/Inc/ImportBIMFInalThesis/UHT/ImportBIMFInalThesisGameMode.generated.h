// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ImportBIMFInalThesisGameMode.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef IMPORTBIMFINALTHESIS_ImportBIMFInalThesisGameMode_generated_h
#error "ImportBIMFInalThesisGameMode.generated.h already included, missing '#pragma once' in ImportBIMFInalThesisGameMode.h"
#endif
#define IMPORTBIMFINALTHESIS_ImportBIMFInalThesisGameMode_generated_h

#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_12_SPARSE_DATA
#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_12_RPC_WRAPPERS
#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_12_ACCESSORS
#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAImportBIMFInalThesisGameMode(); \
	friend struct Z_Construct_UClass_AImportBIMFInalThesisGameMode_Statics; \
public: \
	DECLARE_CLASS(AImportBIMFInalThesisGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ImportBIMFInalThesis"), IMPORTBIMFINALTHESIS_API) \
	DECLARE_SERIALIZER(AImportBIMFInalThesisGameMode)


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAImportBIMFInalThesisGameMode(); \
	friend struct Z_Construct_UClass_AImportBIMFInalThesisGameMode_Statics; \
public: \
	DECLARE_CLASS(AImportBIMFInalThesisGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/ImportBIMFInalThesis"), IMPORTBIMFINALTHESIS_API) \
	DECLARE_SERIALIZER(AImportBIMFInalThesisGameMode)


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	IMPORTBIMFINALTHESIS_API AImportBIMFInalThesisGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AImportBIMFInalThesisGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(IMPORTBIMFINALTHESIS_API, AImportBIMFInalThesisGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AImportBIMFInalThesisGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	IMPORTBIMFINALTHESIS_API AImportBIMFInalThesisGameMode(AImportBIMFInalThesisGameMode&&); \
	IMPORTBIMFINALTHESIS_API AImportBIMFInalThesisGameMode(const AImportBIMFInalThesisGameMode&); \
public: \
	IMPORTBIMFINALTHESIS_API virtual ~AImportBIMFInalThesisGameMode();


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	IMPORTBIMFINALTHESIS_API AImportBIMFInalThesisGameMode(AImportBIMFInalThesisGameMode&&); \
	IMPORTBIMFINALTHESIS_API AImportBIMFInalThesisGameMode(const AImportBIMFInalThesisGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(IMPORTBIMFINALTHESIS_API, AImportBIMFInalThesisGameMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AImportBIMFInalThesisGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AImportBIMFInalThesisGameMode) \
	IMPORTBIMFINALTHESIS_API virtual ~AImportBIMFInalThesisGameMode();


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_9_PROLOG
#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_12_SPARSE_DATA \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_12_RPC_WRAPPERS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_12_ACCESSORS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_12_INCLASS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_12_SPARSE_DATA \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_12_ACCESSORS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_12_INCLASS_NO_PURE_DECLS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> IMPORTBIMFINALTHESIS_API UClass* StaticClass<class AImportBIMFInalThesisGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
