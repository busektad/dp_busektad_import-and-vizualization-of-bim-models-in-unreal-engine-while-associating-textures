// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ImportBIMFInalThesis/Public/ReadAndParseResultFile.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeReadAndParseResultFile() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	IMPORTBIMFINALTHESIS_API UClass* Z_Construct_UClass_UReadAndParseResultFile();
	IMPORTBIMFINALTHESIS_API UClass* Z_Construct_UClass_UReadAndParseResultFile_NoRegister();
	UPackage* Z_Construct_UPackage__Script_ImportBIMFInalThesis();
// End Cross Module References
	void UReadAndParseResultFile::StaticRegisterNativesUReadAndParseResultFile()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(UReadAndParseResultFile);
	UClass* Z_Construct_UClass_UReadAndParseResultFile_NoRegister()
	{
		return UReadAndParseResultFile::StaticClass();
	}
	struct Z_Construct_UClass_UReadAndParseResultFile_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReadAndParseResultFile_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_ImportBIMFInalThesis,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReadAndParseResultFile_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "ReadAndParseResultFile.h" },
		{ "ModuleRelativePath", "Public/ReadAndParseResultFile.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReadAndParseResultFile_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReadAndParseResultFile>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_UReadAndParseResultFile_Statics::ClassParams = {
		&UReadAndParseResultFile::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UReadAndParseResultFile_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReadAndParseResultFile_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReadAndParseResultFile()
	{
		if (!Z_Registration_Info_UClass_UReadAndParseResultFile.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_UReadAndParseResultFile.OuterSingleton, Z_Construct_UClass_UReadAndParseResultFile_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_UReadAndParseResultFile.OuterSingleton;
	}
	template<> IMPORTBIMFINALTHESIS_API UClass* StaticClass<UReadAndParseResultFile>()
	{
		return UReadAndParseResultFile::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReadAndParseResultFile);
	UReadAndParseResultFile::~UReadAndParseResultFile() {}
	struct Z_CompiledInDeferFile_FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_UReadAndParseResultFile, UReadAndParseResultFile::StaticClass, TEXT("UReadAndParseResultFile"), &Z_Registration_Info_UClass_UReadAndParseResultFile, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(UReadAndParseResultFile), 1963318781U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_2232431800(TEXT("/Script/ImportBIMFInalThesis"),
		Z_CompiledInDeferFile_FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
