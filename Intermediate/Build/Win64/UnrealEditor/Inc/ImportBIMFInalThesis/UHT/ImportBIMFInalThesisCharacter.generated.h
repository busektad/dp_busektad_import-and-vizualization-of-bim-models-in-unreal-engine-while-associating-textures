// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ImportBIMFInalThesisCharacter.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef IMPORTBIMFINALTHESIS_ImportBIMFInalThesisCharacter_generated_h
#error "ImportBIMFInalThesisCharacter.generated.h already included, missing '#pragma once' in ImportBIMFInalThesisCharacter.h"
#endif
#define IMPORTBIMFINALTHESIS_ImportBIMFInalThesisCharacter_generated_h

#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_20_SPARSE_DATA
#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetHasRifle); \
	DECLARE_FUNCTION(execSetHasRifle);


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetHasRifle); \
	DECLARE_FUNCTION(execSetHasRifle);


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_20_ACCESSORS
#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAImportBIMFInalThesisCharacter(); \
	friend struct Z_Construct_UClass_AImportBIMFInalThesisCharacter_Statics; \
public: \
	DECLARE_CLASS(AImportBIMFInalThesisCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ImportBIMFInalThesis"), NO_API) \
	DECLARE_SERIALIZER(AImportBIMFInalThesisCharacter)


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_20_INCLASS \
private: \
	static void StaticRegisterNativesAImportBIMFInalThesisCharacter(); \
	friend struct Z_Construct_UClass_AImportBIMFInalThesisCharacter_Statics; \
public: \
	DECLARE_CLASS(AImportBIMFInalThesisCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ImportBIMFInalThesis"), NO_API) \
	DECLARE_SERIALIZER(AImportBIMFInalThesisCharacter)


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AImportBIMFInalThesisCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AImportBIMFInalThesisCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AImportBIMFInalThesisCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AImportBIMFInalThesisCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AImportBIMFInalThesisCharacter(AImportBIMFInalThesisCharacter&&); \
	NO_API AImportBIMFInalThesisCharacter(const AImportBIMFInalThesisCharacter&); \
public: \
	NO_API virtual ~AImportBIMFInalThesisCharacter();


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AImportBIMFInalThesisCharacter(AImportBIMFInalThesisCharacter&&); \
	NO_API AImportBIMFInalThesisCharacter(const AImportBIMFInalThesisCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AImportBIMFInalThesisCharacter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AImportBIMFInalThesisCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AImportBIMFInalThesisCharacter) \
	NO_API virtual ~AImportBIMFInalThesisCharacter();


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_17_PROLOG
#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_20_SPARSE_DATA \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_20_RPC_WRAPPERS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_20_ACCESSORS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_20_INCLASS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_20_SPARSE_DATA \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_20_ACCESSORS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_20_INCLASS_NO_PURE_DECLS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> IMPORTBIMFINALTHESIS_API UClass* StaticClass<class AImportBIMFInalThesisCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
