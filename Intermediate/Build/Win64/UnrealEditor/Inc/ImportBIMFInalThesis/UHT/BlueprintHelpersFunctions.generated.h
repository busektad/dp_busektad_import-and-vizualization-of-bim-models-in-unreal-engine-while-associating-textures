// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "BlueprintHelpersFunctions.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef IMPORTBIMFINALTHESIS_BlueprintHelpersFunctions_generated_h
#error "BlueprintHelpersFunctions.generated.h already included, missing '#pragma once' in BlueprintHelpersFunctions.h"
#endif
#define IMPORTBIMFINALTHESIS_BlueprintHelpersFunctions_generated_h

#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_15_SPARSE_DATA
#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSplitStringByDelimiter);


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSplitStringByDelimiter);


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_15_ACCESSORS
#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUBlueprintHelpersFunctions(); \
	friend struct Z_Construct_UClass_UBlueprintHelpersFunctions_Statics; \
public: \
	DECLARE_CLASS(UBlueprintHelpersFunctions, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ImportBIMFInalThesis"), NO_API) \
	DECLARE_SERIALIZER(UBlueprintHelpersFunctions)


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUBlueprintHelpersFunctions(); \
	friend struct Z_Construct_UClass_UBlueprintHelpersFunctions_Statics; \
public: \
	DECLARE_CLASS(UBlueprintHelpersFunctions, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ImportBIMFInalThesis"), NO_API) \
	DECLARE_SERIALIZER(UBlueprintHelpersFunctions)


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBlueprintHelpersFunctions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBlueprintHelpersFunctions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBlueprintHelpersFunctions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBlueprintHelpersFunctions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBlueprintHelpersFunctions(UBlueprintHelpersFunctions&&); \
	NO_API UBlueprintHelpersFunctions(const UBlueprintHelpersFunctions&); \
public: \
	NO_API virtual ~UBlueprintHelpersFunctions();


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UBlueprintHelpersFunctions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UBlueprintHelpersFunctions(UBlueprintHelpersFunctions&&); \
	NO_API UBlueprintHelpersFunctions(const UBlueprintHelpersFunctions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UBlueprintHelpersFunctions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UBlueprintHelpersFunctions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UBlueprintHelpersFunctions) \
	NO_API virtual ~UBlueprintHelpersFunctions();


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_12_PROLOG
#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_15_SPARSE_DATA \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_15_RPC_WRAPPERS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_15_ACCESSORS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_15_INCLASS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_15_SPARSE_DATA \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_15_ACCESSORS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_15_INCLASS_NO_PURE_DECLS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> IMPORTBIMFINALTHESIS_API UClass* StaticClass<class UBlueprintHelpersFunctions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_BlueprintHelpersFunctions_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
