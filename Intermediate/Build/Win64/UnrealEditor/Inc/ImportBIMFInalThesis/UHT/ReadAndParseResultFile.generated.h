// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ReadAndParseResultFile.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef IMPORTBIMFINALTHESIS_ReadAndParseResultFile_generated_h
#error "ReadAndParseResultFile.generated.h already included, missing '#pragma once' in ReadAndParseResultFile.h"
#endif
#define IMPORTBIMFINALTHESIS_ReadAndParseResultFile_generated_h

#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_15_SPARSE_DATA
#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_15_RPC_WRAPPERS
#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_15_ACCESSORS
#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUReadAndParseResultFile(); \
	friend struct Z_Construct_UClass_UReadAndParseResultFile_Statics; \
public: \
	DECLARE_CLASS(UReadAndParseResultFile, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ImportBIMFInalThesis"), NO_API) \
	DECLARE_SERIALIZER(UReadAndParseResultFile)


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUReadAndParseResultFile(); \
	friend struct Z_Construct_UClass_UReadAndParseResultFile_Statics; \
public: \
	DECLARE_CLASS(UReadAndParseResultFile, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ImportBIMFInalThesis"), NO_API) \
	DECLARE_SERIALIZER(UReadAndParseResultFile)


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReadAndParseResultFile(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UReadAndParseResultFile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReadAndParseResultFile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReadAndParseResultFile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReadAndParseResultFile(UReadAndParseResultFile&&); \
	NO_API UReadAndParseResultFile(const UReadAndParseResultFile&); \
public: \
	NO_API virtual ~UReadAndParseResultFile();


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UReadAndParseResultFile(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UReadAndParseResultFile(UReadAndParseResultFile&&); \
	NO_API UReadAndParseResultFile(const UReadAndParseResultFile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UReadAndParseResultFile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UReadAndParseResultFile); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UReadAndParseResultFile) \
	NO_API virtual ~UReadAndParseResultFile();


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_12_PROLOG
#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_15_SPARSE_DATA \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_15_RPC_WRAPPERS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_15_ACCESSORS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_15_INCLASS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_15_SPARSE_DATA \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_15_ACCESSORS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_15_INCLASS_NO_PURE_DECLS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> IMPORTBIMFINALTHESIS_API UClass* StaticClass<class UReadAndParseResultFile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_Public_ReadAndParseResultFile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
