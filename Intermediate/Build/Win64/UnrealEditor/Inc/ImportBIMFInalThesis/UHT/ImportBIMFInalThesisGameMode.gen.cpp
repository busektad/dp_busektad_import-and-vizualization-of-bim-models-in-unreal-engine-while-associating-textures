// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ImportBIMFInalThesis/ImportBIMFInalThesisGameMode.h"
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeImportBIMFInalThesisGameMode() {}
// Cross Module References
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	IMPORTBIMFINALTHESIS_API UClass* Z_Construct_UClass_AImportBIMFInalThesisGameMode();
	IMPORTBIMFINALTHESIS_API UClass* Z_Construct_UClass_AImportBIMFInalThesisGameMode_NoRegister();
	UPackage* Z_Construct_UPackage__Script_ImportBIMFInalThesis();
// End Cross Module References
	void AImportBIMFInalThesisGameMode::StaticRegisterNativesAImportBIMFInalThesisGameMode()
	{
	}
	IMPLEMENT_CLASS_NO_AUTO_REGISTRATION(AImportBIMFInalThesisGameMode);
	UClass* Z_Construct_UClass_AImportBIMFInalThesisGameMode_NoRegister()
	{
		return AImportBIMFInalThesisGameMode::StaticClass();
	}
	struct Z_Construct_UClass_AImportBIMFInalThesisGameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UECodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UECodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AImportBIMFInalThesisGameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_ImportBIMFInalThesis,
	};
#if WITH_METADATA
	const UECodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AImportBIMFInalThesisGameMode_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering HLOD WorldPartition DataLayers Transformation" },
		{ "IncludePath", "ImportBIMFInalThesisGameMode.h" },
		{ "ModuleRelativePath", "ImportBIMFInalThesisGameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AImportBIMFInalThesisGameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AImportBIMFInalThesisGameMode>::IsAbstract,
	};
	const UECodeGen_Private::FClassParams Z_Construct_UClass_AImportBIMFInalThesisGameMode_Statics::ClassParams = {
		&AImportBIMFInalThesisGameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_AImportBIMFInalThesisGameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AImportBIMFInalThesisGameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AImportBIMFInalThesisGameMode()
	{
		if (!Z_Registration_Info_UClass_AImportBIMFInalThesisGameMode.OuterSingleton)
		{
			UECodeGen_Private::ConstructUClass(Z_Registration_Info_UClass_AImportBIMFInalThesisGameMode.OuterSingleton, Z_Construct_UClass_AImportBIMFInalThesisGameMode_Statics::ClassParams);
		}
		return Z_Registration_Info_UClass_AImportBIMFInalThesisGameMode.OuterSingleton;
	}
	template<> IMPORTBIMFINALTHESIS_API UClass* StaticClass<AImportBIMFInalThesisGameMode>()
	{
		return AImportBIMFInalThesisGameMode::StaticClass();
	}
	DEFINE_VTABLE_PTR_HELPER_CTOR(AImportBIMFInalThesisGameMode);
	AImportBIMFInalThesisGameMode::~AImportBIMFInalThesisGameMode() {}
	struct Z_CompiledInDeferFile_FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_Statics
	{
		static const FClassRegisterCompiledInInfo ClassInfo[];
	};
	const FClassRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_Statics::ClassInfo[] = {
		{ Z_Construct_UClass_AImportBIMFInalThesisGameMode, AImportBIMFInalThesisGameMode::StaticClass, TEXT("AImportBIMFInalThesisGameMode"), &Z_Registration_Info_UClass_AImportBIMFInalThesisGameMode, CONSTRUCT_RELOAD_VERSION_INFO(FClassReloadVersionInfo, sizeof(AImportBIMFInalThesisGameMode), 1770447930U) },
	};
	static FRegisterCompiledInInfo Z_CompiledInDeferFile_FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_4044448913(TEXT("/Script/ImportBIMFInalThesis"),
		Z_CompiledInDeferFile_FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_Statics::ClassInfo, UE_ARRAY_COUNT(Z_CompiledInDeferFile_FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisGameMode_h_Statics::ClassInfo),
		nullptr, 0,
		nullptr, 0);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
