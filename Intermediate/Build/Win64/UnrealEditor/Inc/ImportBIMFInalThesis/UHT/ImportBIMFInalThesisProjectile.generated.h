// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

// IWYU pragma: private, include "ImportBIMFInalThesisProjectile.h"
#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UPrimitiveComponent;
struct FHitResult;
#ifdef IMPORTBIMFINALTHESIS_ImportBIMFInalThesisProjectile_generated_h
#error "ImportBIMFInalThesisProjectile.generated.h already included, missing '#pragma once' in ImportBIMFInalThesisProjectile.h"
#endif
#define IMPORTBIMFINALTHESIS_ImportBIMFInalThesisProjectile_generated_h

#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_15_SPARSE_DATA
#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit);


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit);


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_15_ACCESSORS
#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAImportBIMFInalThesisProjectile(); \
	friend struct Z_Construct_UClass_AImportBIMFInalThesisProjectile_Statics; \
public: \
	DECLARE_CLASS(AImportBIMFInalThesisProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ImportBIMFInalThesis"), NO_API) \
	DECLARE_SERIALIZER(AImportBIMFInalThesisProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAImportBIMFInalThesisProjectile(); \
	friend struct Z_Construct_UClass_AImportBIMFInalThesisProjectile_Statics; \
public: \
	DECLARE_CLASS(AImportBIMFInalThesisProjectile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/ImportBIMFInalThesis"), NO_API) \
	DECLARE_SERIALIZER(AImportBIMFInalThesisProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AImportBIMFInalThesisProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AImportBIMFInalThesisProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AImportBIMFInalThesisProjectile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AImportBIMFInalThesisProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AImportBIMFInalThesisProjectile(AImportBIMFInalThesisProjectile&&); \
	NO_API AImportBIMFInalThesisProjectile(const AImportBIMFInalThesisProjectile&); \
public: \
	NO_API virtual ~AImportBIMFInalThesisProjectile();


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AImportBIMFInalThesisProjectile(AImportBIMFInalThesisProjectile&&); \
	NO_API AImportBIMFInalThesisProjectile(const AImportBIMFInalThesisProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AImportBIMFInalThesisProjectile); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AImportBIMFInalThesisProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AImportBIMFInalThesisProjectile) \
	NO_API virtual ~AImportBIMFInalThesisProjectile();


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_12_PROLOG
#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_15_SPARSE_DATA \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_15_RPC_WRAPPERS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_15_ACCESSORS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_15_INCLASS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_15_SPARSE_DATA \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_15_ACCESSORS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_15_INCLASS_NO_PURE_DECLS \
	FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> IMPORTBIMFINALTHESIS_API UClass* StaticClass<class AImportBIMFInalThesisProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID FID_Users_Tadesas_Documents_Unreal_Projects_ImportBIMFInalThesis_Source_ImportBIMFInalThesis_ImportBIMFInalThesisProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
